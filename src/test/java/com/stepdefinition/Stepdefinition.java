package com.stepdefinition;

import java.net.MalformedURLException;

import org.testng.Assert;

import com.browserFactory.DriverFactory;
import com.pages.AndriodAppPage;
//import com.usbank.pages.UiKitCatalog;
import com.pages.UiKitCatalog;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.AppiumDriver;

public class Stepdefinition {
	
	DriverFactory factory;
	AppiumDriver  driver;
	AndriodAppPage page;
    UiKitCatalog kitPage;
	
	
	
	
	@Given("I initialize the android driver")
	public void i_initialize_the_android_driver() throws MalformedURLException, InterruptedException {
		factory=new DriverFactory();
		driver=factory.initiateDriver("AndriodApp");
		page=new AndriodAppPage(driver);
	    
	}

	@Given("I click on {string} Link after opening demo application")
	public void i_click_on_Link_after_opening_demo_application(String menuItems) throws InterruptedException {
		page.clickOnAnyMenuInDemoApp(menuItems);
	}

	@Then("I validate the {string} should present on the page")
	public void i_validate_the_should_present_on_the_page(String textOnPage) throws InterruptedException {
		String actual=page.getValueFromApp(textOnPage);
        Assert.assertEquals(textOnPage,actual);

	}

	@Then("I click on CheckBox present on the page")
	public void i_click_on_CheckBox_present_on_the_page() {
		page.checkBoxCheckIfNotChecked();
	}

	@Then("I click on {string} on the page")
	public void i_click_on_on_the_page(String wifiSetting) throws InterruptedException {
		page.clickOnWifiSetting(wifiSetting);
	}

	@Then("I enter value in textbox {string}")
	public void i_enter_value_in_textbox(String string) throws InterruptedException {
		page.sendKeysInTextBox(string);
	}
	
	//IOS
	@Given("I click on alertView link on UiKitCatalog page")
	public void i_click_on_alertView_link_on_UiKitCatalog_page() throws MalformedURLException, InterruptedException {
		factory=new DriverFactory();
		driver=factory.initiateDriver("IOS");
		kitPage=new UiKitCatalog(driver);
		kitPage.clicOnLink();
		   Thread.sleep(2000);
		
	}

	@Given("I capatured the text of Simple webelement on the page")
	public void i_capatured_the_text_of_Simple_webelement_on_the_page() {
		kitPage.getTextOnPage();
	}

	
	@After
	public void tearDown() {
		driver.closeApp();
		driver.quit();
	}

}
