package com.pages;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AndriodAppPage {
	
	
	 private String partialXpathPart1="//android.widget.TextView[@text='";
     private String partialXpathPart2="']";
    @FindBy(id = "android:id/checkbox")
     public WebElement checkBox;
    @FindBy(className = "android.widget.EditText")
     public WebElement className;



    AppiumDriver driver;
    

    public AndriodAppPage(AppiumDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }


    public void clickOnAnyMenuInDemoApp(String menuToClick) throws InterruptedException {
        
       AndroidElement element= (AndroidElement) driver.findElementByXPath(partialXpathPart1+menuToClick+partialXpathPart2);
       element.click();

        Thread.sleep(2000);
    }
    public String getValueFromApp(String textToValidate) throws InterruptedException {
        Thread.sleep(2000);
        String wifiText=driver.findElementByXPath(partialXpathPart1+textToValidate+partialXpathPart2).getText();
        
        return wifiText;
    }
    public void checkBoxCheckIfNotChecked()  {
 
        if(checkBox.getAttribute("checked").equals("true")){
        }else {
            //ele.click();
        	checkBox.click();
        }
    }
    public void clickOnWifiSetting(String menuToClick) throws InterruptedException {
        driver.findElementByXPath(partialXpathPart1+menuToClick+partialXpathPart2).click();
        
    }
    public void sendKeysInTextBox(String textToSend) throws InterruptedException {
        Thread.sleep(1000);
        className.sendKeys(textToSend);
        String actual= className.getText();
        System.out.println("Actual in app is"+actual);
        Assert.assertEquals(textToSend,actual);
    }

}
