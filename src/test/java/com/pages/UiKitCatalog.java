package com.pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class UiKitCatalog {
	
	
	@iOSXCUITFindBy(accessibility = "AlertControllerViewController")
    private MobileElement alertVeiwId;
    @iOSXCUITFindBy(accessibility = "Simple")
    private MobileElement simpleElement;
 
 
 
AppiumDriver driver;

 public UiKitCatalog(AppiumDriver driver){
        this.driver=driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }
 
 public void clicOnLink(){
       
	 alertVeiwId.click();

    }
 public void getTextOnPage(){
       
   String text=  simpleElement.getText();
   System.out.println("Text on page is:-"+ text);

    }


}
