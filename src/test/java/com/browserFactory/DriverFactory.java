package com.browserFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverFactory {
	
	    DesiredCapabilities capabilities;
	    AppiumDriverLocalService service;
	    AppiumDriver driver;

	    public AppiumDriver initiateDriver(String nameOfDriver) throws InterruptedException, MalformedURLException {

	        if(nameOfDriver.equals("AndriodApp")){

	            //Thread.sleep(4000);
	            capabilities=new DesiredCapabilities();
	            capabilities.setCapability("no",true);
	            capabilities.setCapability("newCommandTimeout", 100000);
	            capabilities.setCapability("noReset", true);
	            capabilities.setCapability("noRest", true);
	            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel_3a");
	            //capabilities.setCapability(MobileCapabilityType.APP, "C:\\Users\\Navneet\\Desktop\\Appium\\Andriod\\apk\\ApiDemos-debug.apk");
	            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,"io.appium.android.apis");
	            capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,"io.appium.android.apis.ApiDemos");
	            driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

	        }
	        if(nameOfDriver.equals("IOS")) {
	        	capabilities=new DesiredCapabilities();
	        	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 11");
	    		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "IOS");
	    		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
	    		//cap.setCapability(MobileCapabilityType.APP, "//Users//navmaury//Desktop//UIKitCatalog.app");
	    		capabilities.setCapability(MobileCapabilityType.APP, "//Users//navmaury//eclipse-workspace//Demo/project//UIKitCatalog.app");
	    		driver=new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
	        }
	       
	        return driver;
	    }

}
