Feature:  Android, IOS  app testing


@Android
  Scenario: Testing Demo App links
    Given I initialize the android driver
    And I click on "Preference" Link after opening demo application
    And I click on "3. Preference dependencies" Link after opening demo application
    Then I validate the "WiFi settings" should present on the page
    And I click on CheckBox present on the page
    And I click on "WiFi settings" on the page
    Then I enter value in textbox "Hello App"
    
    
 @IOS   
 Scenario: Get text on the IOS Page of Simple WebElement
    Given I click on alertView link on UiKitCatalog page
    And   I capatured the text of Simple webelement on the page