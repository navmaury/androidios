$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/featureFile/andriod.feature");
formatter.feature({
  "name": "Android  app testing",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Testing Demo App links",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Android"
    }
  ]
});
formatter.step({
  "name": "I initialize the android driver",
  "keyword": "Given "
});
formatter.match({
  "location": "Stepdefinition.i_initialize_the_android_driver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"Preference\" Link after opening demo application",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefinition.i_click_on_Link_after_opening_demo_application(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"3. Preference dependencies\" Link after opening demo application",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefinition.i_click_on_Link_after_opening_demo_application(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I validate the \"WiFi settings\" should present on the page",
  "keyword": "Then "
});
formatter.match({
  "location": "Stepdefinition.i_validate_the_should_present_on_the_page(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on CheckBox present on the page",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefinition.i_click_on_CheckBox_present_on_the_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"WiFi settings\" on the page",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefinition.i_click_on_on_the_page(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter value in textbox \"Hello App\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Stepdefinition.i_enter_value_in_textbox(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});